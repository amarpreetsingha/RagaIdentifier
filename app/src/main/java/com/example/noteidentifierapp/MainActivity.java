package com.example.noteidentifierapp;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import static com.example.noteidentifierapp.R.id.audioSample;

public class MainActivity extends Activity {
    private String[] spinnerfreqRange = {"11.025 KHz (Lowest)", "16.000 KHz", "22.050 KHz", "44.100 KHz (Highest)"};
    private Integer[] freqSet = {11025, 16000, 22050, 44100};
    private ArrayAdapter<String> adapter;
    private Spinner spFrequency;
    private Button startRec, stopRec, playBack;
    private Boolean isRecording = false;
    private AudioRecord audioRecord;
    private TextView audioSample;
    private int selectedFrequency ;
    private LineGraphSeries<DataPoint> lineGraphSeries;
    private int audio_samples, minBufferSize;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_CONFIGURATION_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    int BufferElements2Rec = 1024; // want to play 2048 (2K) since 2 bytes we use only 1024
    int BytesPerElement = 2; // 2 bytes in 16bit format
    private Thread recordingThread = null;
    private GraphView graph;
    private double graph2LastXValue = 5d;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startRec = (Button) findViewById(R.id.startrec);
        stopRec = (Button) findViewById(R.id.stoprec);
        playBack = (Button) findViewById(R.id.playback);
        audioSample = (TextView) findViewById(R.id.audioSample);
        spFrequency = (Spinner) findViewById(R.id.frequency);
        graph = (GraphView) findViewById(R.id.graph);
        lineGraphSeries = new LineGraphSeries<>();

        //Initializing graph
        graph.addSeries(lineGraphSeries);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(20);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerfreqRange);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFrequency.setAdapter(adapter);



        spFrequency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedFrequency = freqSet[parent.getSelectedItemPosition()];
                minBufferSize = AudioRecord.getMinBufferSize(selectedFrequency, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);
                Toast.makeText(MainActivity.this, ""+selectedFrequency, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        startRec.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                spFrequency.setEnabled(false);
                startRec.setEnabled(false);
                playBack.setEnabled(false);
                startRecording();

            }
        });

        stopRec.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                spFrequency.setEnabled(true);
                startRec.setEnabled(true);
                playBack.setEnabled(true);
                stopRecording();
            }
        });

        playBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    playAudio(getFilePath());
                }catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


    }

    public void startRecording() {
        audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                selectedFrequency,
                RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING,
                BufferElements2Rec * BytesPerElement);

        audioRecord.startRecording();
        isRecording = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                writeAudioDataToFile();
            }
        }).start();


    }

    //convert short to byte
    private byte[] shortToByte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;

    }


    public void plotGraph(int[] iData) throws InterruptedException {
        for(int i = 0; i < iData.length; i+=5) {
            graph2LastXValue += 0.5d;
            lineGraphSeries.appendData(new DataPoint(graph2LastXValue, iData[i]), true, 40);
            Thread.sleep(50);
        }
    }

    public void writeAudioDataToFile() {
        // Write the output audio in byte

        short sData[] = new short[BufferElements2Rec];

//        FileOutputStream os = null;
//        try {
//            os = new FileOutputStream(getFilePath());
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }

        while (isRecording) {
            // gets the voice output from microphone to byte format

            //Reads audio data from the audio hardware for recording into a short array.
            audioRecord.read(sData, 0, BufferElements2Rec);
            //System.out.println("Short writing to file" + sData.toString());
            try {
                 // writes the data to file from buffer
                 // stores the voice buffer
                byte bData[] = shortToByte(sData);
//                plotGraph(byteToInt(bData));
                centerClipping(byteToInt(bData), 0.3F);
                // Log.d("Bytedata", ""+bData);
                //Log.d("Shortdata", ""+sData);
               // os.write(bData, 0, BufferElements2Rec * BytesPerElement);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
//        try {
//            os.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    private void stopRecording() {
        // stops the recording activity
        if (null != audioRecord) {
            isRecording = false;
            audioRecord.stop();
            audioRecord.release();
            audioRecord = null;
            recordingThread = null;
        }
    }

    public String getFilePath() {
        String exStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        Log.d("MainActivity", "exStoragePath : "+exStoragePath);
        File appTmpPath = new File(exStoragePath + "/RagaIdentifier/");
        boolean isDirectoryCreated = appTmpPath.mkdirs();
        Log.d("MainActivity", "directory "+appTmpPath+" is created : "+isDirectoryCreated);
        String tempFilename = "Charukeshi.wav";
        String filePath = appTmpPath.getAbsolutePath() + File.separator + tempFilename;

        return  filePath;
    }

    private void playAudio(String filePath) throws IOException
    {
        // We keep temporarily filePath globally as we have only two sample sounds now..
        if (filePath == null)
            return;

        //Reading the file..
        byte[] byteData = null;
        File file = null;

        // for ex. path= "/sdcard/samplesound.pcm" or "/sdcard/samplesound.wav"
        file = new File(filePath);
        byteData = new byte[(int) file.length()/7000];

        Log.d("length", ""+file.length());

        FileInputStream in = null;
        try {
            in = new FileInputStream( file );
            in.read( byteData );
            in.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        centerClipping(byteToInt(byteData), 0.3F);
//
//        // Set and push to audio track..
//
//        AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
//                selectedFrequency,
//                RECORDER_CHANNELS,
//                RECORDER_AUDIO_ENCODING,
//                minBufferSize,
//                AudioTrack.MODE_STREAM);
//
//        if (audioTrack != null) {
//            audioTrack.play();
//            // Write the byte array to the track
//            audioTrack.write(byteData, 0, byteData.length);
//            audioTrack.stop();
//            audioTrack.release();
//        }
//        else
//            Log.d("TCAudio", "audio track is not initialised ");

    }


    public void centerClipping(int[] iData, float Cl) {
        float[] dataPoints = new float[iData.length];
        /*
        Taking out maximum from array
        Alternative way : Sort array using array.sort() and max will be (length - 1)
        Alternative way will be more costly because array.sort() uses dual pivot sorting
        method that has time complexity upto O(nlogn) or O(n2).
         */

        int data = iData[0];
        for(int i = 1; i <  iData.length; i++) {
            if(data < iData[i]) {
                data = iData[i];
            }

        }

        /*
        Normalising data points to float
         */

        for(int i = 0; i < iData.length; i++) {
            dataPoints[i] = (float)iData[i] / (float)data;
          }

        /*
        data contains maximum of array
         */

        for(int i = 0; i < iData.length; i++) {
            if(Math.abs(dataPoints[i]) <= Cl )
                dataPoints[i] = 0;
            if(dataPoints[i] > Cl)
                dataPoints[i] = dataPoints[i] - Cl;
            if(dataPoints[i] < -Cl)
                dataPoints[i] = dataPoints[i] + Cl;

            Log.d("pt", ""+dataPoints[i]);

        }


    }

    public int[] byteToInt(byte[] bData) {
        int[] iData = new int[bData.length];
        for(int i = 0; i < bData.length; i++) {
            iData[i] = (int)bData[i];
        }

        return iData;
    }
}
